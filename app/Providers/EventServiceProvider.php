<?php

namespace App\Providers;

use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Contracts\Events\Dispatcher;
use App\Models\CardOpening;
use App\Models\CardSimple;
use App\Models\CardOuter;
use App\Models\CardComplex;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\CardCreating' => [
            'App\Listeners\SetCardType'
        ],
        'App\Events\CardCreated' => [
            'App\Listeners\CreateCardIndex'
        ],
        'App\Events\CardUpdated' => [
            'App\Listeners\UpdateCardIndex'
        ],
    ];
}
