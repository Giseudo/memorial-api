<?php

namespace App\Events;

class CardCreating extends Event
{
    public $card;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($card)
    {
        $this->card = $card;
    }
}
