<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('oauth:manage_user');
    }

    /**
     * Paginate resource.
     *
     * @return void
     */
    public function index()
    {
        return response(User::paginate(10), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return [\AppModels\OAuth\User]
     */
    public function show($id)
    {
        return response(User::find($id), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Models\OAuth\User
     */
    public function store(Request $request)
    {
        $input = $request->input('user');
        $password = $request->input('password');
        $user = new User();

        $user->nome = $input['nome'];
        $user->email = $input['email'];
        $user->senha = sha1($password);
        $user->save();

        return response($user, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \App\Models\OAuth\User
     */
    public function update(Request $request, $id)
    {
        $input = $request->input('user');
        $password = $request->input('password');
        $user = User::find($id);

        if (!empty($password)) {
            $user->senha = sha1($password);
        }

        $user->nome = $input['nome'];
        $user->email = $input['email'];
        $user->save();

        return response($user, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return boolean
     */
    public function destroy($id)
    {
        return response(User::destroy($id), 200);
    }
}
