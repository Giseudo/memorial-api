<?php

namespace App\Http\Controllers\OAuth;

use App\Http\Controllers\Controller;
use App\Models\OAuth\Client;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('oauth:manage_oauth');
    }

    /**
     * Paginate resource.
     *
     * @return void
     */
    public function index()
    {
        return response(Client::with('scopes')->paginate(10));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return [\AppModels\OAuth\Client]
     */
    public function show($id)
    {
        return response(Client::with('scopes')->find($id));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Models\OAuth\Client
     */
    public function store(Request $request)
    {
        $input = $request->input('client');
        $client = new Client();

        $client->id = $input['id'];
        $client->name = $input['name'];
        $client->secret = $input['secret'];
        $client->save();

        $client->scopes()->sync($input['scopes']);

        return response($client, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \App\Models\OAuth\Client
     */
    public function update(Request $request, $id)
    {
        $input = $request->input('client');
        $client = Client::find($id);

        $client->name = $input['name'];
        $client->secret = $input['secret'];
        $client->save();

        $client->scopes()->sync($input['scopes']);

        return response($client, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return boolean
     */
    public function destroy($id)
    {
        return response(Client::destroy($id), 200);
    }
}
