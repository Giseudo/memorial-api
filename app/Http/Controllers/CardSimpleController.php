<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CardSimple;

class CardSimpleController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('oauth:manage_card');
    }

    /**
     * Paginate resource.
     *
     * @return void
     * @return [\App\Models\CardSimple]
     */
    public function index() {
        $cards = CardSimple::paginate();

        return response($cards, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \App\Models\CardSimple
     */
    public function show($id) {
        $card = CardSimple::find($id);

        return response($card, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Models\CardSimple
     */
    public function store(Request $request) {
        $input = $request->input('card');

        try {
            $card = CardSimple::create($input);
            
            dd($card);
        } catch (\Exception $e) {
            return response($e->getMessage(), 401);
        }

        return response($card, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \App\Models\CardSimple
     */
    public function update(Request $request, $id) {
        $input = $request->input('card');

        try {
            $card = CardSimple::find($id);
            $card->update($input);
        } catch (\Exception $e) {
            return response($e->getMessage(), 401);
        }

        return response($card, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return boolean
     */
    public function destroy($id) {
        return response(CardSimple::destroy($id), 200);
    }
}
