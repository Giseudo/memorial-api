<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use App\Models\Card;
use Illuminate\Http\Request;

class TagController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('oauth:manage_tags');
    }

    /**
     * Paginate resource.
     *
     * @return void
     */
    public function index() {
        return response(Tag::paginate(10), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return [\App\Models\OAuth\Timeline]
     */
    public function show($id) {
      return response(Tag::find($id), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Models\OAuth\Timeline
     */
    public function store(Request $request) {
        $input_tag = $request->input('tag');
//        $input_card = $request->input('card');

        $tag = new Tag();
        $tag->name = $input_tag['name'];
        $tag->save();
        
        //$cards = array de objetos do tipo Card
//      $tag->card()->saveMany($cards);
        

        return response($tag, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \App\Models\OAuth\Timeline
     */
    public function update(Request $request, $id) {
        $input = $request->input('tag');
//        $input_card = $request->input('card');

        $tag = Tag::find($id);

        $tag->name = $input['name'];
        $tag->save();
        
//        if($timeline->id){
//            $card = new CardOpening($input_card);
//            $card->save();
//        }

        return response($tag, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return boolean
     */
    public function destroy($id) {
        return response(Tag::destroy($id), 200);
    }

}
