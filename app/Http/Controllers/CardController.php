<?php

namespace App\Http\Controllers;

use App\Models\Card;
use Illuminate\Http\Request;
use Carbon\Carbon;

class CardController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('oauth:manage_card');
    }

    /**
     * Paginate resource.
     *
     * @return void
     */
    public function index() {
        $cards = Card::with('timeline')
            ->paginate();

        return response($cards, 200);
    }

    /**
     * Paginate latest updated/created resources.
     *
     * @return void
     */
    public function latest() {
        $cards = Card::latest()
            ->with('timeline')
            ->paginate(10);

        return response($cards, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return [\App\Models\OAuth\Timeline]
     */
    public function show($id) {
        $card = Card::with('timeline')
            ->find($id);

        return response($card, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return boolean
     */
    public function destroy($id) {
        return response(Card::destroy($id), 200);
    }

    /**
     * Do a search in the database
     *
     * @param  string   $q
     * @return \App\Models\Card
     */
    public function search(Request $request) {
        $input = $request->input();

        // Model query
        $cards = Card::query();

        // Timeline filter
        if (isset($input['timeline']))
            $cards = $cards->where('timeline_id', $input['timeline']);

        // Card type filter
        if (isset($input['tipo']))
            $cards = $cards->where('tipo', $input['tipo']);

        // Intervalo inicio filter
        if (isset($input['intervalo_inicio']))
            $cards = $cards->where('intervalo_inicio', '>=', $input['intervalo_inicio']);

        // Intervalo fim filter
        if (isset($input['intervalo_fim']))
            $cards = $cards->where('intervalo_fim', '<=', $input['intervalo_fim']);

        // Last updated filter
        if (isset($input['updated'])) {
            $begin = Carbon::createFromFormat('Y-m-d', $input['updated'])->format('Y-m-d');
            $end = Carbon::createFromFormat('Y-m-d', $input['updated'])->addDay()->format('Y-m-d');

            $cards = $cards->whereBetween('created_at', [$begin, $end])
                ->orWhereBetween('updated_at', [$begin, $end]);
        }

        // Query filter
        if (isset($input['q'])) {
            $matching = Card::search($input['q'])->get()->pluck('id');
            $cards = $cards->whereIn('id', $matching);
        }

        return response($cards->with('timeline')->paginate(), 200);
    }
}
