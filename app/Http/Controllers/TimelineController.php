<?php

namespace App\Http\Controllers;

use App\Models\Timeline;
use App\Models\CardOpening;
use App\Models\Card;
use Illuminate\Http\Request;

class TimelineController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('oauth:manage_timeline');
    }

    /**
     * Paginate resource.
     *
     * @return void
     */
    public function index() {
        return response(Timeline::paginate(10), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return [\App\Models\OAuth\Timeline]
     */
    public function show($id) {
        $timeline = Timeline::find($id);
        $opening = $timeline->abertura;

        return response([
            'timeline' => $timeline,
            'opening' => $opening
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Models\OAuth\Timeline
     */
    public function store(Request $request) {
        $input = $request->all();

        try {
            $timeline = Timeline::create($input['timeline']);

            if ($timeline->id)
                $input['opening']['timeline_id'] = $timeline->id;

            $opening = CardOpening::create($input['opening']);

            return response([
                'timeline' => $timeline,
                'opening' => $opening,
            ], 200);
        } catch (\Exception $e) {
            return response($e->getMessage(), 401);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \App\Models\OAuth\Timeline
     */
    public function update(Request $request, $id) {
        $input = $request->all();

        try {
            $timeline = Timeline::find($id);
            $opening = $timeline->abertura;

            $timeline->update($input['timeline']);

            if ($opening)
                $opening->update($input['opening']);
        } catch (\Exception $e) {
            return response($e->getMessage(), 401);
        }

        return response([
            'timeline' => $timeline,
            'opening' => $opening
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return boolean
     */
    public function destroy($id) {
        $timeline = Timeline::find($id);
        $opening = $timeline->abertura;

        $opening->delete();
        $timeline->delete();

        return response([], 200);
    }
}
