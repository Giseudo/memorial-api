<?php

namespace App\Listeners;

use App\Events\CardCreating;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\CardOpening;
use App\Models\CardSimple;
use App\Models\CardComplex;
use App\Models\CardExtra;
use App\Models\CardOuter;

class SetCardType
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Set the type of card.
     *
     * @param  ExampleEvent  $event
     * @return void
     */
    public function handle(CardCreating $event)
    {
        switch(true) {
        case $event->card instanceof CardOpening:
            $event->card->tipo = 'abertura';
            break;
        case $event->card instanceof CardSimple:
            $event->card->tipo = 'simples';
            break;
        case $event->card instanceof CardComplex:
            $event->card->tipo = 'complexo';
            break;
        case $event->card instanceof CardOuter:
            $event->card->tipo = 'externo';
            break;
        case $event->card instanceof CardExtra:
            $event->card->tipo = 'extra';
            break;
        }
    }
}
