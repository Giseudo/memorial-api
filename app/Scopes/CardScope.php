<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class CardScope implements Scope
{
    /**
     * The type of card.
     *
     * @var string
     */
    private $type;

    /**
     * Apply the scope to a given Eloquent query builder.
     * @param  abertura|simples|complexo|extra|externo  $type
     */
    public function __construct($type = '')
    {
        $this->type = $type;
    }

    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        if($this->type){
            $builder->where('tipo', $this->type);                
        }
        
        $builder->where('pagina_principal_id', NULL);
    }
}
