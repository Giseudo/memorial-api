<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tags';
    
    
    protected $fillable = [
        'name_br',
        'name_en',
        'name_es'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'name'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    
    public function card(){
        return $this->belongsToMany('App\Models\Card', 'cards_tag', 'tag_id', 'card_id');
    }
    
    public function getNameAttribute($value) {
        return $this->attributes['name_' . config('lang.current')];
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name_' . config('lang.current')] = $value;
    }
}
