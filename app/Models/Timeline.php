<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Timeline extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'timelines';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'titulo',
        'slug',
        'bibliografia',
        'fechamento_titulo',
        'fechamento_descricao',
        'episodios',
        'inicio',
        'fim',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'titulo',
        'bibliografia',
        'fechamento_titulo',
        'fechamento_descricao',
        'episodios',
        'simples',
        'complexo',
        'extra',
        'externo'
    ];

    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = [
        'id',
        'titulo',
        'slug',
        'bibliografia',
        'fechamento_titulo',
        'fechamento_descricao',
        'inicio',
        'fim',
        'episodios',
        'simples',
        'complexo',
        'extra',
        'externo',
        'construcao',
        'visitacao',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Get the opening card associated with the card.
     *
     * @return \App\Models\CardOpening
     */
    public function abertura() {
        return $this->hasOne('App\Models\CardOpening', 'timeline_id', 'id');
    }

    public function getTituloAttribute($value) {
        return $this->attributes['titulo_' . config('lang.current')];
    }

    public function setTituloAttribute($value) {
        $this->attributes['titulo_' . config('lang.current')] = $value;
    }

    public function getBibliografiaAttribute($value) {
        return json_decode($this->attributes['bibliografia_' . config('lang.current')]);
    }

    public function setBibliografiaAttribute($value) {
        $this->attributes['bibliografia_' . config('lang.current')] = json_encode($value);
    }
    
    public function getFechamentoTituloAttribute($value) {
        return json_decode($this->attributes['fechamento_titulo_' . config('lang.current')]);
    }

    public function setFechamentoTituloAttribute($value) {
        $this->attributes['fechamento_titulo_' . config('lang.current')] = json_encode($value);
    }

    public function getFechamentoDescricaoAttribute($value) {
        return json_decode($this->attributes['fechamento_descricao_' . config('lang.current')]);
    }

    public function setFechamentoDescricaoAttribute($value) {
        $this->attributes['fechamento_descricao_' . config('lang.current')] = json_encode($value);
    }

    public function getEpisodiosAttribute($value) {
        return $this->attributes['episodios_' . config('lang.current')];
    }

    public function setEpisodiosAttribute($value) {
        $this->attributes['episodios_' . config('lang.current')] = $value;
    }

    public function getSimplesAttribute(){
        $cards = \DB::table('cards')
            ->select('tipo', \DB::raw('count(*) as total'))
            ->where('tipo', 'simples')
            ->where('status', 'publico')
            ->where('timeline_id', $this->attributes['id'])
            ->get();
        
        return $cards[0]->total;
    }
    
    public function getComplexoAttribute(){
        $cards = \DB::table('cards')
            ->select('tipo', \DB::raw('count(*) as total'))
            ->where('tipo', 'complexo')
            ->where('status', 'publico')
            ->where('timeline_id', $this->attributes['id'])
            ->get();
        
        return $cards[0]->total;
    }
    
    public function getExternoAttribute(){
        $cards = \DB::table('cards')
            ->select('tipo', \DB::raw('count(*) as total'))
            ->where('tipo', 'externo')
            ->where('status', 'publico')
            ->where('timeline_id', $this->attributes['id'])
            ->get();
        
        return $cards[0]->total;
    }
    
    public function getExtraAttribute(){
        $cards = \DB::table('cards')
            ->select('tipo', \DB::raw('count(*) as total'))
            ->where('tipo', 'extra')
            ->where('pagina_principal_id', NULL)
            ->where('status', 'publico')
            ->where('timeline_id', $this->attributes['id'])
            ->get();
        
        return $cards[0]->total;
    }
}
