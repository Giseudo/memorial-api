<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Events\CardCreating;
use Laravel\Scout\Searchable;
use App\Scopes\CardScope;

class Card extends Model {

    use Searchable;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cards';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'data',
        'descricao_data',
        'descricao_busca',
        'titulo',
        'credito_foto_capa',
        'slug',
        'subtitulo',
        'texto',
        'tipo',
        'intervalo_inicio',
        'intervalo_fim',
        'url',
        'timeline_id'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'titulo',
        'texto',
        'subtitulo',
        'descricao_data',
        'descricao_busca',
        'credito_foto_capa'
    ];

    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = [
        'id',
        'data',
        'tipo',
        'descricao_data',
        'descricao_busca',
        'titulo',
        'credito_foto_capa',
        'slug',
        'subtitulo',
        'texto',
        'intervalo_inicio',
        'intervalo_fim',
        'url',
        'timeline',
        'timeline_id',
        'created_at',
        'updated_at'
    ];

    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $events = [
        'creating' => CardCreating::class,
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new CardScope());
    }

    /**
     * Get the index name for the model.
     *
     * @return string
     */
    public function searchableAs()
    {
        return 'cards_' . config('lang.current');
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $array = [
            'id' => $this->id,
            'titulo' => $this->titulo,
            'texto' => $this->texto
        ];

        return $array;
    }

    /**
     * Return latest updated cards.
     *
     * @return [\App\Models\Card]
     */
    public static function latest($timeline = NULL){
        $cards = Card::when($timeline, function ($query) use ($timeline) {
            return $query->where('timeline_id', $timeline);
        })
            ->orderBy('created_at', 'updated_at');

        return $cards;
    }

    /**
     * Get the timeline associated with the card.
     *
     * @return \App\Models\Timeline
     */
    public function timeline() {
        return $this->belongsTo('App\Models\Timeline');
    }

    /**
     * Get the background file associated with the card.
     *
     * @return \App\Models\File
     */
    public function background_file() {
        return $this->hasOne('App\Models\File', 'background_file_id', 'id');
    }

    /**
     * Get the cover file associated with the card.
     *
     * @return \App\Models\File
     */
    public function capa_file() {
        return $this->hasOne('App\Models\File', 'capa_file_id', 'id');
    }

    /**
     * Get the current language title.
     *
     * @return string
     */
    public function getTituloAttribute($value) {
        return $this->attributes['titulo_' . config('lang.current')];
    }

    /**
     * Set the current language title.
     */
    public function setTituloAttribute($value) {
        $this->attributes['titulo_' . config('lang.current')] = $value;
    }

    /**
     * Get the current language subtitle.
     *
     * @return string
     */
    public function getSubtituloAttribute($value) {
        return $this->attributes['subtitulo_' . config('lang.current')];
    }

    /**
     * Set the current language subtitle.
     */
    public function setSubtituloAttribute($value) {
        $this->attributes['subtitulo_' . config('lang.current')] = $value;
    }

    /**
     * Get the current language text.
     *
     * @return string
     */
    public function getTextoAttribute($value) {
        return json_decode($this->attributes['texto_' . config('lang.current')]);
    }

    /**
     * Set the current language text.
     */
    public function setTextoAttribute($value) {
        $this->attributes['texto_' . config('lang.current')] = json_encode($value);
    }

    /**
     * Get the current language date description.
     *
     * @return string
     */
    public function getDescricaoDataAttribute($value) {
        return $this->attributes['descricao_data_' . config('lang.current')];
    }

    /**
     * Set the current language date description.
     */
    public function setDescricaoDataAttribute($value) {
        $this->attributes['descricao_data_' . config('lang.current')] = $value;
    }

    /**
     * Get the current language search description.
     *
     * @return string
     */
    public function getDescricaoBuscaAttribute($value) {
        return $this->attributes['descricao_busca_' . config('lang.current')];
    }

    /**
     * Set the current language search description.
     */
    public function setDescricaoBuscaAttribute($value) {
        $this->attributes['descricao_busca_' . config('lang.current')] = $value;
    }

    /**
     * Get the current language credit cover caption.
     *
     * @return string
     */
    public function getCreditoFotoCapaAttribute($value) {
        return $this->attributes['credito_foto_capa_' . config('lang.current')];
    }

    /**
     * Set the current language credit cover caption.
     */
    public function setCreditoFotoCapaAttribute($value) {
        $this->attributes['credito_foto_capa_' . config('lang.current')] = $value;
    }


    public static function getCardByTimeline($timeline_id){
        return Card::where('tipo','abertura')->where('timeline_id', $timeline_id)->get();
    }
}
