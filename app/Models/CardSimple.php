<?php

namespace App\Models;

use App\Scopes\CardScope;

class CardSimple extends Card {

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new CardScope('simples'));
    }

    /**
     * Get the thumbnail data file associated with the card.
     *
     * @return \App\Models\File
     */
    public function thumbnail_data_file() {
        return $this->hasOne('App\Models\File', 'thumbnail_data_file_id', 'id');
    }

    /**
     * Get the thumbnail data capa file associated with the card.
     *
     * @return \App\Models\File
     */
    public function thumbnail_data_capa_video_file() {
        return $this->hasOne('App\Models\File', 'thumbnail_data_capa_video_file_id', 'id');
    }
}
