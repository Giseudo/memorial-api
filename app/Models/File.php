<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class File extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'arquivos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'slug',
        'nome_original',
        'nome_arquivo',
        'extensao',
        'tipo_mime',
        'tamanho',
        'largura_imagem',
        'altura_imagem',
        'data_criacao',
        'data_modificacao'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

}
