<?php

namespace App\Models;

class CardExtra extends Card {
    /**
     * Get the tags associated with the card.
     *
     * @return [\App\Models\Tag]
     */
    public function tags() {
        return $this->belongsToMany('App\Models\Tag', 'cards_tag', 'card_id', 'tag_id');
    }
}
