<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimelineFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('timelines', function (Blueprint $table) {
            $table->string('episodios_br', 50);
            $table->string('episodios_es', 50);
            $table->string('episodios_en', 50);

            $table->string('fechamento_titulo_br', 50);
            $table->string('fechamento_titulo_es', 50);
            $table->string('fechamento_titulo_en', 50);

            $table->text('fechamento_descricao_br');
            $table->text('fechamento_descricao_es');
            $table->text('fechamento_descricao_en');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('timelines', function($table) {
            $table->dropColumn('episodios_br');
            $table->dropColumn('episodios_en');
            $table->dropColumn('episodios_es');

            $table->dropColumn('fechamento_titulo_br');
            $table->dropColumn('fechamento_titulo_en');
            $table->dropColumn('fechamento_titulo_es');

            $table->dropColumn('fechamento_descricao_br');
            $table->dropColumn('fechamento_descricao_en');
            $table->dropColumn('fechamento_descricao_es');

            $table->dropColumn('deleted_at');
            $table->dropTimestamps();
        });
    }
}
